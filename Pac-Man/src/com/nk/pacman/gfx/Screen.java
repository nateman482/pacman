package com.nk.pacman.gfx;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class Screen {
	
	public final int width, height;
	public final Graphics g;
	
	private final BufferedImage[] sprites;
	
	public Screen(int width, int height, Graphics g) {
		this.width = width;
		this.height = height;
		this.g = g;
		
		g.setFont(new Font("Arial", Font.PLAIN, 14));
		
		sprites = new SpriteSheet().getSpritesheet("/img/sprites.png");
	}
	
	public void draw(int id, int x, int y) {
		g.drawImage(sprites[id], x, y, null);
	}
	
	public void write(String msg, int x, int y) {
		g.drawString(msg, x, y);
	}
	
	public void drawRect(int x, int y, int w, int h, boolean filled, Color color) {
		g.setColor(color);
		if (filled) {
			g.fillRect(x, y, w, h);
		} else {
			g.drawRect(x, y, w, h);
		}
	}
}
