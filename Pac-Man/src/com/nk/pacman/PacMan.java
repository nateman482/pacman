package com.nk.pacman;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.util.Random;

import javax.swing.JFrame;

import com.nk.pacman.game.Game;
import com.nk.pacman.gfx.Screen;

public class PacMan extends Canvas implements Runnable {

	private static final long serialVersionUID = 1L;
	private static JFrame frame;
	public static int WIDTH = 240;
	public static int HEIGHT = 160; // 720 x 480
	public static int SCALE = 3;
	public static final String TITLE = "RPG";
	
	public static void main(String[] args) {
		PacMan main = new PacMan();
		main.setMinimumSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
		main.setMaximumSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
		main.setPreferredSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
		
		frame = new JFrame(TITLE);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		frame.add(main);
		frame.pack();
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		main.start();
	}

	private boolean running = false;
	
	private BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
	private Input input;
	private Screen screen;
	private Game game;
	
	public static Random r = new Random();
	
	private void start() {
		super.requestFocus();
		running = true;
		new Thread(this).start();
	}
	
	private void stop() {
		running = false;
	}
	
	private void init() {
		input = new Input(this);
		screen = new Screen(WIDTH, HEIGHT, image.getGraphics());
		game = new Game(this, input, screen);
	}
	
	@Override
	public void run() {
		long lastTime = System.nanoTime();
		double unprocessed = 0;
		double nsPerTick = 1000000000.0 / 60;
//		int ticks = 0;
//		long lastTimer1 = System.currentTimeMillis();

		init();

		while (running) {
			long now = System.nanoTime();
			unprocessed += (now - lastTime) / nsPerTick;
			lastTime = now;

			while (unprocessed >= 1) {
//				ticks++;
				tick();
				render();
				unprocessed = 0;
			}
			
			try {
				Thread.sleep(0);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

//			if (System.currentTimeMillis() - lastTimer1 > 1000) {
//				lastTimer1 += 1000;
//				System.out.println(ticks + " ticks");
//				ticks = 0;
//			}
		} System.exit(0);
	}
	
	private void tick() {
		input.tick();
		game.tick();
	}

	private void render() {
		frame.pack();
		
		BufferStrategy bs = getBufferStrategy();
		if (bs == null) {
			this.createBufferStrategy(3);
			return;
		}

		Graphics g = image.getGraphics();

		g.setColor(Color.BLACK);
		g.fillRect(0, 0, WIDTH, HEIGHT);

		game.draw();

		g = bs.getDrawGraphics();
		g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
		bs.show();
	}
}
