package com.nk.pacman.state;

public enum MenuState {
	MAIN(0), OPTIONS(1);
	
	public int id;
	
	MenuState(int id) {
		this.id = id;
	}
}
