package com.nk.pacman.gfx;

import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

public class SpriteSheet {

	public BufferedImage[] getSpritesheet(String path) {
		BufferedImage sheet = null;
		BufferedImage[] sprites;

		URL url = Toolkit.getDefaultToolkit().getClass().getResource(path);
		
		try {
			sheet = ImageIO.read(url);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		int rows = sheet.getHeight() / 16;
		int cols = sheet.getWidth() / 16;
		sprites = new BufferedImage[rows * cols];

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				sprites[(i * cols) + j] = sheet.getSubimage(i * 16, j * 16, 16, 16);
			}
		}

		return sprites;
	}
}
