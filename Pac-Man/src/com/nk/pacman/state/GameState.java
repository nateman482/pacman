package com.nk.pacman.state;

public enum GameState {
	MENU(0), INGAME(1);
	
	public int id;
	
	GameState(int id) {
		this.id = id;
	}
}
