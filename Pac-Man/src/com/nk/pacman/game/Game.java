package com.nk.pacman.game;

import com.nk.pacman.Input;
import com.nk.pacman.PacMan;
import com.nk.pacman.gfx.Screen;
import com.nk.pacman.state.GameState;
import com.nk.pacman.state.MenuState;

public class Game {
	
	public final PacMan main;
	public final Input input;
	
	private int state = GameState.MENU.id;
	private int menuState = MenuState.MAIN.id;
	private Map map;
	
	private int totalMenuOptions = 3, menuIndex = 0, totalOptionsMenuOptions = 2;
	
	public Game(PacMan main, Input input, Screen screen) {
		this.main = main;
		this.input = input;
	}
	
	public void tick() {
		
	}
	
	public void draw() {
		
	}
}
