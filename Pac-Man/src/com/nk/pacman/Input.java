package com.nk.pacman;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Input implements KeyListener {
	
	public Input(PacMan main) {
		main.addKeyListener(this);
	}

	public boolean[] downkeys = new boolean[65536];
	public boolean[] typedkeys = new boolean[65536];
	public boolean[] togglekeys = new boolean[65536];
	public boolean[] busykeys = new boolean[65536];
	public boolean[] movekeys = new boolean[65536];
	
	public void tick() {
		for (int i = 0; i < 65536; i++) {
			if (togglekeys[i]) {
				togglekeys[i] = false;
				busykeys[i] = true;
			} else if (downkeys[i] && !togglekeys[i] && !busykeys[i]) {
				togglekeys[i] = true;
			}
			
			if (busykeys[i]) {
				if (!downkeys[i]) {
					busykeys[i] = false;
				}
			}
		}
	}
	
	@Override
	public void keyPressed(KeyEvent ke) {
		downkeys[ke.getKeyCode()] = true;
	}

	@Override
	public void keyReleased(KeyEvent ke) {
		downkeys[ke.getKeyCode()] = false;
	}

	@Override
	public void keyTyped(KeyEvent ke) {
		typedkeys[ke.getKeyCode()] = true;
	}
}
